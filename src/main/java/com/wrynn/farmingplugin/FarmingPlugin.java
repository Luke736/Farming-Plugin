package com.wrynn.farmingplugin;

import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.wrynn.farmingplugin.commands.Commands;
import com.wrynn.farmingplugin.listeners.*;

import java.util.logging.Logger;

public final class FarmingPlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic
        PluginDescriptionFile pdfFile = getDescription();
        Logger logger = getLogger();

        loadConfigManager();
        registerEvents();
        registerCommands();

        logger.info(pdfFile.getName() + " has been enabled. (V: " + pdfFile.getVersion() + ")");
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        PluginDescriptionFile pdfFile = getDescription();
        Logger logger = getLogger();

        logger.info(pdfFile.getName() + " has been disabled. (V: " + pdfFile.getVersion() + ")");
    }

    public void loadConfigManager(){
        ConfigManager cfgm = new ConfigManager();
        cfgm.Playerssetup();
        cfgm.Settingssetup();
    }

    public void registerEvents(){
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new BlockBreak(),this);
        pm.registerEvents(new OnJoin(), this);
        pm.registerEvents(new InventoryClick(), this);
        pm.registerEvents(new BlockPlace(), this);
        pm.registerEvents(new PlayerInteract(), this);
        pm.registerEvents(new BlockFromTo(), this);
        pm.registerEvents(new PistonExtend(), this);
    }

    public void registerCommands(){
        getCommand("farming").setExecutor(new Commands());
    }
}

package com.wrynn.farmingplugin;

import java.util.UUID;

public class LevelManager {

    private UUID uuid;

    public LevelManager(UUID Uuid){
        uuid = Uuid;
    }

    public void setExperience(int value){
        ConfigManager cfgm = new ConfigManager();
        cfgm.Playerssetup();
        cfgm.getPlayers().set("skills." + uuid + ".farming.experience", value);
        cfgm.savePlayers();
    }

    public void addExperience(int value){
        ConfigManager cfgm = new ConfigManager();
        cfgm.Playerssetup();
        cfgm.getPlayers().set("skills." + uuid + ".farming.experience", (getExperience() + value));
        cfgm.savePlayers();
    }

    public int getExperience(){
        ConfigManager cfgm = new ConfigManager();
        cfgm.Playerssetup();
        return cfgm.getPlayers().getInt("skills." + uuid + ".farming.experience");
    }

    public int getLevel(){
        ConfigManager cfgm = new ConfigManager();
        cfgm.Playerssetup();
        return cfgm.getPlayers().getInt("skills." +uuid + ".farming.level");
    }

    public void setLevel(int value){
        ConfigManager cfgm = new ConfigManager();
        cfgm.Playerssetup();
        cfgm.getPlayers().set("skills." + uuid + ".farming.level", value);
        cfgm.savePlayers();
    }

    public boolean checkLevel(){
        int level = getLevel();
        int experience = getExperience();

        if(level == 1 && experience >= 1000){
            setLevel(2);
            setExperience(experience - 1000);
            return true;
        }

        if(level == 2 && experience >= 5000){
            setLevel(3);
            setExperience(experience - 5000);
            return true;
        }

        if(level == 3 && experience >= 25000){
            setLevel(4);
            setExperience(experience - 25000);
            return true;
        }

        if(level == 4 && experience >= 125000){
            setLevel(5);
            setExperience(experience - 125000);
            return true;
        }

        if(level == 5 && experience >= 625000){
            setLevel(6);
            setExperience(experience - 625000);
            return true;
        }

        return false;
    }

    public int getLevelBound(){
        switch(getLevel()){
            case 1: return 1000;
            case 2: return 5000;
            case 3: return 25000;
            case 4: return 125000;
            case 5: return 625000;
            default: return 0;
        }
    }
}

package com.wrynn.farmingplugin;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class ConfigManager {

    private FarmingPlugin plugin = FarmingPlugin.getPlugin(FarmingPlugin.class);

    // Files & File Configs Here
    public FileConfiguration playerscfg;
    private File playersfile;
    public FileConfiguration settingscfg;
    private File settingsfile;
    // --------------------------

    public void Playerssetup() {
        if (!plugin.getDataFolder().exists()) {
            plugin.getDataFolder().mkdir();
        }

        playersfile = new File(plugin.getDataFolder(), "players.yml");

        if (!playersfile.exists()) {
            try {
                playersfile.createNewFile();
            } catch (IOException e) {
                Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "Could not create the players.yml file");
            }
        }
        playerscfg = YamlConfiguration.loadConfiguration(playersfile);
    }

    public FileConfiguration getPlayers() {
        return playerscfg;
    }

    public void savePlayers() {
        try {
            playerscfg.save(playersfile);

        } catch (IOException e) {
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "Could not save the players.yml file");
        }
    }

    public void Settingssetup() {
        if (!plugin.getDataFolder().exists()) {
            plugin.getDataFolder().mkdir();
        }

        boolean exists = true;

        settingsfile = new File(plugin.getDataFolder(), "settings.yml");

        if (!settingsfile.exists()) {
            try {
                settingsfile.createNewFile();
            } catch (IOException e) {
                Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "Could not create the settings.yml file");
            }
            exists = false;
        }
        settingscfg = YamlConfiguration.loadConfiguration(settingsfile);

        if(!exists){
            //default values if the config files have been deleted/haven't been created.
            settingscfg.set("experience.BEETROOT_BLOCK", 1);
            settingscfg.set("experience.CROPS", 1);
            settingscfg.set("experience.POTATO", 1);
            settingscfg.set("experience.CARROT", 1);
            settingscfg.set("experience.MELON_BLOCK", 1);
            settingscfg.set("experience.PUMPKIN", 1);
            settingscfg.set("experience.SUGAR_CANE_BLOCK", 1);
            settingscfg.set("experience.CACTUS", 1);
            settingscfg.set("experience.COCOA", 1);
            settingscfg.set("experience.NETHER_WARTS", 1);
            settingscfg.set("crop-trample", false);
            settingscfg.set("drops.seeds.WOOD_HOE", 1);
            settingscfg.set("drops.seeds.STONE_HOE", 1);
            settingscfg.set("drops.seeds.IRON_HOE", 2);
            settingscfg.set("drops.seeds.GOLD_HOE", 2);
            settingscfg.set("drops.seeds.DIAMOND_HOE", 3);
            settingscfg.set("drops.crops.WOOD_HOE", 1);
            settingscfg.set("drops.crops.STONE_HOE", 1);
            settingscfg.set("drops.crops.IRON_HOE", 2);
            settingscfg.set("drops.crops.GOLD_HOE", 2);
            settingscfg.set("drops.crops.DIAMOND_HOE", 3);
            saveSettings();
        }
    }

    public FileConfiguration getSettings() {
        return settingscfg;
    }

    public void saveSettings() {
        try {
            settingscfg.save(settingsfile);

        } catch (IOException e) {
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "Could not save the settings.yml file");
        }
    }
}

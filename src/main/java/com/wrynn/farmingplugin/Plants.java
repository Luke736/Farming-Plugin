package com.wrynn.farmingplugin;

import org.bukkit.Material;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class Plants {

    private Material blocktype;

    public Plants(Material Blocktype){
        blocktype = Blocktype;
    }

    public ItemStack getItem(){

        ItemStack i;
        Material m;

        switch(blocktype){
            case BEETROOT_BLOCK: m = Material.BEETROOT; break;
            case CROPS: m = Material.WHEAT; break;
            case POTATO: m = Material.POTATO_ITEM; break;
            case CARROT: m = Material.CARROT_ITEM; break;
            case MELON_BLOCK: m = Material.MELON; break;
            case PUMPKIN: m = Material.PUMPKIN; break;
            case SUGAR_CANE_BLOCK: m = Material.SUGAR_CANE; break;
            case CACTUS: m = Material.CACTUS; break;
            case COCOA: m = Material.INK_SACK; break;
            case NETHER_WARTS: m = Material.NETHER_STALK; break;
            default: m = Material.AIR; break;
        }

        if(m == Material.INK_SACK)
            i = new ItemStack(m, 1, (byte) 3);
        else
            i= new ItemStack(m, 1);

        return i;
    }

    public ItemStack getSeeds(){

        ItemStack i;
        Material m;

        switch(blocktype){
            case BEETROOT_BLOCK: m = Material.BEETROOT_SEEDS; break;
            case CROPS: m = Material.SEEDS; break;
            case POTATO: m = Material.POTATO_ITEM; break;
            case CARROT: m = Material.CARROT_ITEM; break;
            case MELON_BLOCK: m = Material.MELON_SEEDS; break;
            case PUMPKIN: m = Material.PUMPKIN_SEEDS; break;
            case SUGAR_CANE_BLOCK: m = Material.SUGAR_CANE; break;
            case CACTUS: m = Material.CACTUS; break;
            case COCOA: m = Material.INK_SACK; break;
            case NETHER_WARTS: m = Material.NETHER_STALK; break;
            default: m = Material.AIR; break;
        }

        if(m == Material.INK_SACK)
            i = new ItemStack(m, 1, (byte) 3);
        else
            i= new ItemStack(m, 1);

        return i;
    }

    public int getMaxGrowthStage(){

        switch(blocktype){
            case COCOA: return 8;
            case CROPS: case POTATO: case CARROT: return 7;
            case BEETROOT_BLOCK: case NETHER_WARTS: return 3;
            default: return 0;
        }
    }

    public void DropFlowers(BlockBreakEvent event){

        ConfigManager cfgm = new ConfigManager();
        cfgm.Playerssetup();

        if(cfgm.getPlayers().getBoolean("skills." + event.getPlayer().getUniqueId() + ".farming.flower-drop")) {
            ItemStack flower;
            Random rand = new Random();
            int flowerID = rand.nextInt(10);
            if (rand.nextInt(10) < 3) {
                if (flowerID == 9)
                    flower = new ItemStack(Material.YELLOW_FLOWER, 1);
                else
                    flower = new ItemStack(Material.RED_ROSE, 1, (byte) flowerID);

            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), flower);
            }
        }
    }
}

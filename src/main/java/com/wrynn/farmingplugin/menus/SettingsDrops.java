package com.wrynn.farmingplugin.menus;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import com.wrynn.farmingplugin.ConfigManager;
import com.wrynn.farmingplugin.FarmingPlugin;

import java.util.ArrayList;

public class SettingsDrops implements Listener {

    private Plugin plugin = FarmingPlugin.getPlugin(FarmingPlugin.class);

    public void newInventory(Player player){
        ConfigManager cfgm = new ConfigManager();
        cfgm.Settingssetup();

        Inventory I = plugin.getServer().createInventory(null, 9, ChatColor.GOLD + "" + ChatColor.BOLD + "Drops Settings");

        //Information / Help
        ItemStack help = new ItemStack(Material.CONCRETE, 1, (byte) 5);
        ItemMeta helpmeta = help.getItemMeta();
        helpmeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Instructions");
        ArrayList<String> helplore = new ArrayList<String>();
        helplore.add(ChatColor.WHITE + "Left click increases by 1");
        helplore.add(ChatColor.WHITE + "Right click decreases by 1");
        helplore.add(ChatColor.WHITE + "Hold shift to modify seed drops.");
        helplore.add(ChatColor.WHITE + "Seed drops number will not affect crops that don't have any relative seed.");
        helpmeta.setLore(helplore);
        helpmeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        help.setItemMeta(helpmeta);

        //Wooden Hoe
        ItemStack wood = new ItemStack(Material.WOOD_HOE, 1);
        ItemMeta woodmeta = wood.getItemMeta();
        woodmeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Wooden Hoe");
        ArrayList<String> woodlore = new ArrayList<>();
        woodlore.add(ChatColor.WHITE + "Crops: " + ChatColor.GOLD + cfgm.getSettings().getInt("drops.crops.WOOD_HOE"));
        woodlore.add(ChatColor.WHITE + "Seeds: " + ChatColor.GOLD + cfgm.getSettings().getInt("drops.seeds.WOOD_HOE"));
        woodmeta.setLore(woodlore);
        woodmeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        wood.setItemMeta(woodmeta);

        //Stone Hoe
        ItemStack stone = new ItemStack(Material.STONE_HOE, 1);
        ItemMeta stonemeta = stone.getItemMeta();
        stonemeta.setDisplayName(ChatColor.GRAY + "" + ChatColor.BOLD + "Stone Hoe");
        ArrayList<String> stonelore = new ArrayList<>();
        stonelore.add(ChatColor.WHITE + "Crops: " + ChatColor.GOLD + cfgm.getSettings().getInt("drops.crops.STONE_HOE"));
        stonelore.add(ChatColor.WHITE + "Seeds: " + ChatColor.GOLD + cfgm.getSettings().getInt("drops.seeds.STONE_HOE"));
        stonemeta.setLore(stonelore);
        stonemeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        stone.setItemMeta(stonemeta);

        //Iron Hoe
        ItemStack iron = new ItemStack(Material.IRON_HOE, 1);
        ItemMeta ironmeta = iron.getItemMeta();
        ironmeta.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + "Iron Hoe");
        ArrayList<String> ironlore = new ArrayList<>();
        ironlore.add(ChatColor.WHITE + "Crops: " + ChatColor.GOLD + cfgm.getSettings().getInt("drops.crops.IRON_HOE"));
        ironlore.add(ChatColor.WHITE + "Seeds: " + ChatColor.GOLD + cfgm.getSettings().getInt("drops.seeds.IRON_HOE"));
        ironmeta.setLore(ironlore);
        ironmeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        iron.setItemMeta(ironmeta);

        //Gold Hoe
        ItemStack gold = new ItemStack(Material.GOLD_HOE, 1);
        ItemMeta goldmeta = gold.getItemMeta();
        goldmeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Golden Hoe");
        ArrayList<String> goldlore = new ArrayList<>();
        goldlore.add(ChatColor.WHITE + "Crops: " + ChatColor.GOLD + cfgm.getSettings().getInt("drops.crops.GOLD_HOE"));
        goldlore.add(ChatColor.WHITE + "Seeds: " + ChatColor.GOLD + cfgm.getSettings().getInt("drops.seeds.GOLD_HOE"));
        goldmeta.setLore(goldlore);
        goldmeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        gold.setItemMeta(goldmeta);

        //Diamond Hoe
        ItemStack diamond = new ItemStack(Material.DIAMOND_HOE, 1);
        ItemMeta diamondmeta = diamond.getItemMeta();
        diamondmeta.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "Diamond Hoe");
        ArrayList<String> diamondlore = new ArrayList<>();
        diamondlore.add(ChatColor.WHITE + "Crops: " + ChatColor.GOLD + cfgm.getSettings().getInt("drops.crops.DIAMOND_HOE"));
        diamondlore.add(ChatColor.WHITE + "Seeds: " + ChatColor.GOLD + cfgm.getSettings().getInt("drops.seeds.DIAMOND_HOE"));
        diamondmeta.setLore(diamondlore);
        diamondmeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        diamond.setItemMeta(diamondmeta);

        //Back Button
        ItemStack back = new ItemStack(Material.WOOL, 1, (byte) 14);
        ItemMeta backmeta = back.getItemMeta();
        backmeta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "Back");
        ArrayList<String> backlore = new ArrayList<String>();
        backlore.add(ChatColor.WHITE + "Click to go back!");
        backmeta.setLore(backlore);
        backmeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        back.setItemMeta(backmeta);

        I.setItem(0, help);
        I.setItem(2, wood);
        I.setItem(3, stone);
        I.setItem(4, iron);
        I.setItem(5, gold);
        I.setItem(6, diamond);
        I.setItem(8, back);

        player.openInventory(I);
    }

}

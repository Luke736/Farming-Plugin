package com.wrynn.farmingplugin.menus;


import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import com.wrynn.farmingplugin.ConfigManager;
import com.wrynn.farmingplugin.LevelManager;
import com.wrynn.farmingplugin.FarmingPlugin;

import java.util.ArrayList;
import java.util.UUID;

public class Skills implements Listener {
    private Plugin plugin = FarmingPlugin.getPlugin(FarmingPlugin.class);

    public void newInventory(Player player){

        ConfigManager cfgm = new ConfigManager();
        cfgm.Playerssetup();
        LevelManager lvlm = new LevelManager(player.getUniqueId());

        UUID uuid = player.getUniqueId();

        int experience = cfgm.playerscfg.getInt("skills." + uuid + ".farming");
        Inventory I = plugin.getServer().createInventory(null, 9, ChatColor.GOLD + "" + ChatColor.BOLD + "Farming");

        //Experience/Level display
        ItemStack hoe = new ItemStack(Material.IRON_HOE,1);
        ItemMeta hoemeta= hoe.getItemMeta();
        hoemeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Farming Skill");
        ArrayList<String> hoelore = new ArrayList<String>();
        hoelore.add(ChatColor.WHITE + "Level: " + lvlm.getLevel());

        if(lvlm.getLevelBound() != 0)
            hoelore.add(ChatColor.WHITE + "Experience: "+lvlm.getExperience()+" / "+lvlm.getLevelBound());
        else
            hoelore.add(ChatColor.WHITE + "Experience: MAX / MAX");

        hoemeta.setLore(hoelore);
        hoemeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        hoe.setItemMeta(hoemeta);

        //Flower Drop
        ItemStack flower = new ItemStack(Material.RED_ROSE,1);
        ItemMeta flowermeta = flower.getItemMeta();
        flowermeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Flower Drop");
        ArrayList<String> flowerlore = new ArrayList<String>();
        flowerlore.add(ChatColor.WHITE + "State: " +ChatColor.GOLD + cfgm.getPlayers().getBoolean("skills." + player.getUniqueId() + ".farming.flower-drop"));

        flowermeta.setLore(flowerlore);
        flowermeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        flower.setItemMeta(flowermeta);

        I.setItem(3, hoe);
        I.setItem(5, flower);
        player.openInventory(I);
    }
}

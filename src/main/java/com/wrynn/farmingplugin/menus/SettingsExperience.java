package com.wrynn.farmingplugin.menus;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import com.wrynn.farmingplugin.ConfigManager;
import com.wrynn.farmingplugin.FarmingPlugin;

import java.util.ArrayList;
import java.util.UUID;

public class SettingsExperience implements Listener {

    private Plugin plugin = FarmingPlugin.getPlugin(FarmingPlugin.class);

    public void newInventory(Player player) {

        ConfigManager cfgm = new ConfigManager();
        cfgm.Settingssetup();

        UUID uuid = player.getUniqueId();
        Inventory I = plugin.getServer().createInventory(null, 18, ChatColor.GOLD + "" + ChatColor.BOLD + "Experience Settings");

        //WHEAT
        ItemStack wheat = new ItemStack(Material.WHEAT, 1);
        ItemMeta wheatmeta = wheat.getItemMeta();
        wheatmeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Wheat Experience");
        ArrayList<String> wheatlore = new ArrayList<String>();
        wheatlore.add(ChatColor.WHITE + "Experience: " +ChatColor.GOLD+ cfgm.getSettings().getInt("experience.CROPS"));
        wheatmeta.setLore(wheatlore);
        wheatmeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        wheat.setItemMeta(wheatmeta);

        //POTATO
        ItemStack potato = new ItemStack(Material.POTATO_ITEM, 1);
        ItemMeta potatometa = potato.getItemMeta();
        potatometa.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Potato Experience");
        ArrayList<String> potatolore = new ArrayList<String>();
        potatolore.add(ChatColor.WHITE + "Experience: " +ChatColor.GOLD+ cfgm.getSettings().getInt("experience.POTATO"));
        potatometa.setLore(potatolore);
        potatometa.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        potato.setItemMeta(potatometa);

        //CARROT
        ItemStack carrot = new ItemStack(Material.CARROT_ITEM, 1);
        ItemMeta carrotmeta = carrot.getItemMeta();
        carrotmeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Carrot Experience");
        ArrayList<String> carrotlore = new ArrayList<String>();
        carrotlore.add(ChatColor.WHITE + "Experience: " +ChatColor.GOLD+ cfgm.getSettings().getInt("experience.CARROT"));
        carrotmeta.setLore(carrotlore);
        carrotmeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        carrot.setItemMeta(carrotmeta);

        //BEETROOT
        ItemStack beetroot = new ItemStack(Material.BEETROOT, 1);
        ItemMeta beetrootmeta = beetroot.getItemMeta();
        beetrootmeta.setDisplayName(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Beetroot Experience");
        ArrayList<String> beetrootlore = new ArrayList<String>();
        beetrootlore.add(ChatColor.WHITE + "Experience: "+ChatColor.GOLD + cfgm.getSettings().getInt("experience.BEETROOT_BLOCK"));
        beetrootmeta.setLore(beetrootlore);
        beetrootmeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        beetroot.setItemMeta(beetrootmeta);

        //MELON
        ItemStack melon = new ItemStack(Material.MELON_BLOCK, 1);
        ItemMeta melonmeta = melon.getItemMeta();
        melonmeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Melon Experience");
        ArrayList<String> melonlore = new ArrayList<String>();
        melonlore.add(ChatColor.WHITE + "Experience: " +ChatColor.GOLD+ cfgm.getSettings().getInt("experience.MELON_BLOCK"));
        melonmeta.setLore(melonlore);
        melonmeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        melon.setItemMeta(melonmeta);

        //PUMPKIN
        ItemStack pumpkin = new ItemStack(Material.PUMPKIN, 1);
        ItemMeta pumpkinmeta = pumpkin.getItemMeta();
        pumpkinmeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Pumpkin Experience");
        ArrayList<String> pumpkinlore = new ArrayList<String>();
        pumpkinlore.add(ChatColor.WHITE + "Experience: "+ChatColor.GOLD + cfgm.getSettings().getInt("experience.PUMPKIN"));
        pumpkinmeta.setLore(pumpkinlore);
        pumpkinmeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        pumpkin.setItemMeta(pumpkinmeta);

        //SUGAR CANE
        ItemStack sugarcane = new ItemStack(Material.SUGAR_CANE, 1);
        ItemMeta sugarcanemeta = sugarcane.getItemMeta();
        sugarcanemeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Sugarcane Experience");
        ArrayList<String> sugarcanelore = new ArrayList<String>();
        sugarcanelore.add(ChatColor.WHITE + "Experience: "+ChatColor.GOLD + cfgm.getSettings().getInt("experience.SUGAR_CANE_BLOCK"));
        sugarcanemeta.setLore(sugarcanelore);
        sugarcanemeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        sugarcane.setItemMeta(sugarcanemeta);

        //CACTUS
        ItemStack cactus = new ItemStack(Material.CACTUS, 1);
        ItemMeta cactusmeta = cactus.getItemMeta();
        cactusmeta.setDisplayName(ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "Cactus Experience");
        ArrayList<String> cactuslore = new ArrayList<String>();
        cactuslore.add(ChatColor.WHITE + "Experience: " +ChatColor.GOLD+ cfgm.getSettings().getInt("experience.CACTUS"));
        cactusmeta.setLore(cactuslore);
        cactusmeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        cactus.setItemMeta(cactusmeta);

        //COCOA BEANS
        ItemStack cocoa = new ItemStack(Material.INK_SACK, 1, (byte) 3);
        ItemMeta cocoameta = cocoa.getItemMeta();
        cocoameta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Cocoa Experience");
        ArrayList<String> cocoalore = new ArrayList<String>();
        cocoalore.add(ChatColor.WHITE + "Experience: " +ChatColor.GOLD+ cfgm.getSettings().getInt("experience.COCOA"));
        cocoameta.setLore(cocoalore);
        cocoameta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        cocoa.setItemMeta(cocoameta);

        //NETHERWART
        ItemStack netherwarts = new ItemStack(Material.NETHER_STALK, 1);
        ItemMeta netherwartsmeta = netherwarts.getItemMeta();
        netherwartsmeta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "Netherwart Experience");
        ArrayList<String> netherwartslore = new ArrayList<String>();
        netherwartslore.add(ChatColor.WHITE + "Experience: " +ChatColor.GOLD+ cfgm.getSettings().getInt("experience.NETHER_WARTS"));
        netherwartsmeta.setLore(netherwartslore);
        netherwartsmeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        netherwarts.setItemMeta(netherwartsmeta);

        //BACK BUTTON
        ItemStack back = new ItemStack(Material.WOOL, 1, (byte) 14);
        ItemMeta backmeta = back.getItemMeta();
        backmeta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "Back");
        ArrayList<String> backlore = new ArrayList<String>();
        backlore.add(ChatColor.WHITE + "Click to go back!");
        backmeta.setLore(backlore);
        backmeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        back.setItemMeta(backmeta);

        I.setItem(0, wheat);
        I.setItem(1, carrot);
        I.setItem(2, potato);
        I.setItem(3, beetroot);
        I.setItem(4, cactus);
        I.setItem(5, sugarcane);
        I.setItem(6, pumpkin);
        I.setItem(7, melon);
        I.setItem(8, cocoa);
        I.setItem(9, netherwarts);
        I.setItem(17, back);

        player.openInventory(I);
    }
}

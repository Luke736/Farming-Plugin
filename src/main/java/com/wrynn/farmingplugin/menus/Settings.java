package com.wrynn.farmingplugin.menus;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import com.wrynn.farmingplugin.ConfigManager;
import com.wrynn.farmingplugin.FarmingPlugin;

import java.util.ArrayList;
import java.util.UUID;

public class Settings implements Listener {
    private Plugin plugin = FarmingPlugin.getPlugin(FarmingPlugin.class);

    public void newInventory(Player player){

        ConfigManager cfgm = new ConfigManager();
        cfgm.Settingssetup();

        UUID uuid = player.getUniqueId();
        Inventory I = plugin.getServer().createInventory(null, 9, ChatColor.GOLD + "" + ChatColor.BOLD + "Farming Settings");

        //Crop-Trampling
        ItemStack farmland = new ItemStack(Material.SOIL, 1);
        ItemMeta farmlandmeta = farmland.getItemMeta();
        farmlandmeta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "Crop-Trampling");
        ArrayList<String> farmlandlore = new ArrayList<String>();
        farmlandlore.add(ChatColor.WHITE + "Recommended to be kept false.");
        farmlandlore.add(ChatColor.WHITE + "State: " +ChatColor.GOLD+ cfgm.getSettings().getBoolean("crop-trample"));
        farmlandmeta.setLore(farmlandlore);
        farmlandmeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        farmland.setItemMeta(farmlandmeta);

        //Experience Settings
        ItemStack experience = new ItemStack(Material.ENDER_PEARL, 1);
        ItemMeta experiencemeta = experience.getItemMeta();
        experiencemeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Experience Settings");
        ArrayList<String> experiencelore = new ArrayList<String>();
        experiencelore.add(ChatColor.WHITE + "Click to access experience settings!");
        experiencemeta.setLore(experiencelore);
        experiencemeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        experience.setItemMeta(experiencemeta);

        //Drops Settings
        ItemStack drops = new ItemStack(Material.WHEAT, 1);
        ItemMeta dropsmeta = drops.getItemMeta();
        dropsmeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Drops Settings");
        ArrayList<String> dropslore = new ArrayList<String>();
        dropslore.add(ChatColor.WHITE + "Click to access drops settings!");
        dropsmeta.setLore(dropslore);
        dropsmeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        drops.setItemMeta(dropsmeta);

        I.setItem(2, experience);
        I.setItem(4, drops);
        I.setItem(6, farmland);

        player.openInventory(I);
    }
}

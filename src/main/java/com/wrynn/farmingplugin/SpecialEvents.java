package com.wrynn.farmingplugin;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Zombie;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Random;

public class SpecialEvents {

    public void getRandomBreakEvent(BlockBreakEvent event, Material hoe){
        LevelManager lvlm = new LevelManager(event.getPlayer().getUniqueId());
        int level = lvlm.getLevel();

        //randomiser means there is only 1/100 chance of getting an event
        Random rnd = new Random();
        int random = rnd.nextInt(99);

        if(random == 0) {

            //the event is chosen here.
            int eventrandom = rnd.nextInt(4);

            if (eventrandom == 0 && hoe == Material.DIAMOND_HOE && level == 6)
                lighting(event);

            if(eventrandom == 1)
                GoldenApple(event);

            if(eventrandom == 2)
                PineCone(event);

            if(eventrandom == 3)
                monster(event);
        }

    }

    private void lighting(BlockBreakEvent event){
        event.getBlock().getWorld().strikeLightningEffect(event.getBlock().getLocation());

        ItemStack specialhoe = new ItemStack(Material.DIAMOND_HOE, 1);
        ItemMeta hoemeta = specialhoe.getItemMeta();
        hoemeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Hoe of Destiny!");
        ArrayList<String> hoelore = new ArrayList<>();
        hoelore.add(ChatColor.WHITE + "A gift given from the farming gods above!");
        hoelore.add(ChatColor.WHITE + "Use it wisely, as there is no telling what shall come!");
        hoemeta.setLore(hoelore);

        Random rnd = new Random();
        int enchantmentlevel = rnd.nextInt(2) + 1;

        hoemeta.addEnchant(Enchantment.LOOT_BONUS_BLOCKS, enchantmentlevel, false);
        specialhoe.setItemMeta(hoemeta);

        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation().add(0,1,0), specialhoe);
        event.getPlayer().sendMessage(ChatColor.GOLD + "The farming gods have smiled upon you today and have bestowed a gift upon you!");
    }

    private void GoldenApple(BlockBreakEvent event){
        event.getPlayer().sendMessage(ChatColor.GOLD +""+ ChatColor.BOLD +"Whilst farming the crops a small golden apple has materialised out of thin air!");
        ItemStack goldenapple = new ItemStack(Material.GOLDEN_APPLE, 1);
        ItemMeta goldenapplemeta = goldenapple.getItemMeta();
        goldenapplemeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Golden Apple");
        ArrayList<String> goldenapplelore = new ArrayList<>();
        goldenapplelore.add(ChatColor.WHITE + "An amazing feat of farming!");
        goldenapplemeta.setLore(goldenapplelore);
        goldenapple.setItemMeta(goldenapplemeta);

        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation().add(0, 1, 0), goldenapple);
    }

    private void monster(BlockBreakEvent event){
        Location loc = event.getBlock().getLocation();
        event.getPlayer().sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Your poor farming attempt has angered the gods! They have summoned a wheat monster to punish you.");

        Zombie zombie = (Zombie) loc.getWorld().spawnEntity(loc, EntityType.ZOMBIE);
        zombie.setCustomName(ChatColor.RED + "Wheat Monster!");
        zombie.setCustomNameVisible(true);
    }

    private void PineCone(BlockBreakEvent event){
        ItemStack pinecone = new ItemStack(Material.WOOD_BUTTON, 1);
        ItemMeta pineconemeta = pinecone.getItemMeta();
        pineconemeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Pinecone");
        ArrayList<String> pineconelore = new ArrayList<>();
        pineconelore.add(ChatColor.WHITE + "Just an ordinary pinecone!");
        pineconemeta.setLore(pineconelore);
        pinecone.setItemMeta(pineconemeta);

        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation().add(0, 1, 0), pinecone);
    }
}

package com.wrynn.farmingplugin.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.wrynn.farmingplugin.menus.Settings;
import com.wrynn.farmingplugin.menus.Skills;

public class Commands implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        if (!(sender instanceof Player)) {
            sender.sendMessage("You are not a player!");
            return false;
        }

        Player player = (Player) sender;

        try {
            switch (args[0]) {
                case "skill":
                    Skills skills = new Skills();
                    skills.newInventory(player);
                    return true;
                case "settings":
                    if(player.isOp()) {
                        Settings settings = new Settings();
                        settings.newInventory(player);
                        return true;
                    }
                    else {
                        player.sendMessage(ChatColor.RED + "" + "You do not have permission to use this command!");
                        return false;
                    }
                default:
                    player.sendMessage(ChatColor.RED + "" + "Please enter a menu name! /farming [skill/settings]");
                    break;
            }
        }
        catch(ArrayIndexOutOfBoundsException e){
            player.sendMessage(ChatColor.RED + "" + "Please enter a menu name! /farming [skill/settings]");
        }
        return false;
    }
}

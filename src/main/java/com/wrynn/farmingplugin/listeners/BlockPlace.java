package com.wrynn.farmingplugin.listeners;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockPlace implements Listener {

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event){

        //Entire class prevents crops that have no growth states from being abused.

        Material blocktype = event.getBlockPlaced().getType();
        if(!(blocktype == Material.SUGAR_CANE_BLOCK || blocktype == Material.CACTUS || blocktype == Material.PUMPKIN || blocktype == Material.MELON_BLOCK) || event.getPlayer().isOp())
            return;

        Material blockbeneath = event.getBlockPlaced().getLocation().add(0, -1 , 0).getBlock().getType();

        if(blockbeneath == Material.SUGAR_CANE_BLOCK && event.getBlockPlaced().getType() == Material.SUGAR_CANE_BLOCK){
            event.setCancelled(true);
            event.getPlayer().sendMessage(ChatColor.RED + "You may not place sugarcane blocks atop one another. Please ask a moderator to do so for you.");
        }

        if(blockbeneath == Material.CACTUS && event.getBlockPlaced().getType() == Material.CACTUS){
            event.setCancelled(true);
            event.getPlayer().sendMessage(ChatColor.RED + "You may not place cactus blocks atop one another. Please ask a moderator to do so for you.");
        }

        if(blocktype == Material.PUMPKIN){
            for (BlockFace blockface : BlockFace.values())
                if (event.getBlock().getRelative(blockface).getType() == Material.PUMPKIN_STEM){
                    event.setCancelled(true);
                    event.getPlayer().sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "You may not place pumpkin blocks beside pumpkin stems. Please ask a moderator to do so for you.");
                }
        }

        if(blocktype == Material.MELON_BLOCK){
            for (BlockFace blockface : BlockFace.values())
                if (event.getBlock().getRelative(blockface).getType() == Material.MELON_STEM){
                    event.setCancelled(true);
                    event.getPlayer().sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "You may not play melon blocks beside melon stems. Please ask a moderator to do so for you.");
                }
        }

        if(event.getBlock().getType().name().equals(ChatColor.AQUA + "" + ChatColor.BOLD + "Diamond Flower")){
            if(event.getBlock().getLocation().subtract(0, 1, 0).getBlock().getType() == Material.GRASS || event.getBlock().getLocation().subtract(0, 1, 0).getBlock().getType() == Material.DIRT){
                event.getBlock().getLocation().subtract(0,1,0).getBlock().setType(Material.DIAMOND_BLOCK);
            }
        }
    }
}

package com.wrynn.farmingplugin.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.wrynn.farmingplugin.ConfigManager;

import java.util.UUID;

public class OnJoin implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event){
        UUID uuid = event.getPlayer().getUniqueId();

        ConfigManager cfgm = new ConfigManager();
        cfgm.Playerssetup();

        String puuid = "skills." + uuid;

        //sets up a player's experience config in the players file if they have never joined the server before.
        if (cfgm.playerscfg.getString(puuid) == null) {
            cfgm.getPlayers().set("skills." + uuid + ".farming.experience", 0);
            cfgm.getPlayers().set("skills." + uuid + ".farming.level", 1);
            cfgm.getPlayers().set("skills." + uuid + ".farming.flower-drop", true);
            cfgm.savePlayers();
        }
    }
}

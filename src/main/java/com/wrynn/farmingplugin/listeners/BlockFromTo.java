package com.wrynn.farmingplugin.listeners;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFromToEvent;

public class BlockFromTo implements Listener {
    @EventHandler
    public void onBlockFromTo(BlockFromToEvent event){

        Material blocktype = event.getToBlock().getType();

        //prevents water from destroying crops
        if(blocktype == Material.CROPS || blocktype == Material.POTATO || blocktype == Material.BEETROOT_BLOCK || blocktype == Material.CARROT || blocktype == Material.NETHER_WARTS)
            event.getToBlock().setType(Material.AIR);
    }
}

package com.wrynn.farmingplugin.listeners;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import com.wrynn.farmingplugin.ConfigManager;
import com.wrynn.farmingplugin.menus.Settings;
import com.wrynn.farmingplugin.menus.SettingsDrops;
import com.wrynn.farmingplugin.menus.SettingsExperience;
import com.wrynn.farmingplugin.menus.Skills;

public class InventoryClick implements Listener {
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event){

        if(event.getClickedInventory() == null)
            return;

        //different if statements for the different menus in the plugin.

        if(event.getClickedInventory().getName().equals(ChatColor.GOLD + "" + ChatColor.BOLD + "Experience Settings")){
            event.setCancelled(true);
            experiencesettings(event);
        }

        if(event.getClickedInventory().getName().equals(ChatColor.GOLD + "" + ChatColor.BOLD + "Farming")){
            event.setCancelled(true);
            skills(event);
        }

        if(event.getClickedInventory().getName().equals(ChatColor.GOLD + "" + ChatColor.BOLD + "Drops Settings")){
            event.setCancelled(true);
            dropssettings(event);
        }

        if(event.getClickedInventory().getName().equals(ChatColor.GOLD + "" + ChatColor.BOLD + "Farming Settings")){
            event.setCancelled(true);
            settings(event);
        }

    }

    private void experiencesettings(InventoryClickEvent event){

        Material item = event.getCurrentItem().getType();
        Player player = (Player) event.getWhoClicked();
        ConfigManager cfgm = new ConfigManager();
        cfgm.Settingssetup();
        int experiencechange = 0;
        boolean back = false;

        SettingsExperience settings = new SettingsExperience();

        //identifies which click is used and sets experience change accordingly
        if(event.getClick().isRightClick())
            experiencechange = -1;

        if(event.getClick().isLeftClick())
            experiencechange = 1;

        if(event.getClick().isRightClick() && event.getClick().isShiftClick())
            experiencechange = -10;

        if(event.getClick().isLeftClick() && event.getClick().isShiftClick())
            experiencechange = 10;

        //modifies experience values according to selection.
        switch(item){
            case WHEAT: cfgm.getSettings().set("experience.CROPS", cfgm.getSettings().getInt("experience.CROPS") + experiencechange);  break;
            case POTATO_ITEM: cfgm.getSettings().set("experience.POTATO", cfgm.getSettings().getInt("experience.POTATO") + experiencechange);  break;
            case CARROT_ITEM: cfgm.getSettings().set("experience.CARROT", cfgm.getSettings().getInt("experience.CARROT") + experiencechange);  break;
            case BEETROOT: cfgm.getSettings().set("experience.BEETROOT_BLOCK", cfgm.getSettings().getInt("experience.BEETROOT_BLOCK") + experiencechange);  break;
            case SUGAR_CANE: cfgm.getSettings().set("experience.SUGAR_CANE_BLOCK", cfgm.getSettings().getInt("experience.SUGAR_CANE_BLOCK") + experiencechange);  break;
            case MELON_BLOCK: cfgm.getSettings().set("experience.MELON_BLOCK", cfgm.getSettings().getInt("experience.MELON_BLOCK") + experiencechange);  break;
            case PUMPKIN: cfgm.getSettings().set("experience.PUMPKIN", cfgm.getSettings().getInt("experience.PUMPKIN") + experiencechange);  break;
            case CACTUS: cfgm.getSettings().set("experience.CACTUS", cfgm.getSettings().getInt("experience.CACTUS") + experiencechange);  break;
            case NETHER_STALK: cfgm.getSettings().set("experience.NETHER_WARTS", cfgm.getSettings().getInt("experience.NETHER_WARTS") + experiencechange);  break;
            case INK_SACK: cfgm.getSettings().set("experience.COCOA", cfgm.getSettings().getInt("experience.COCOA") + experiencechange);  break;
            case WOOL: Settings settings1 = new Settings(); settings1.newInventory(player); back = true; break;
        }
        cfgm.saveSettings();
        if(!back)
            settings.newInventory(player);
    }

    private void skills(InventoryClickEvent event){
        ConfigManager cfgm = new ConfigManager();
        cfgm.Playerssetup();
        Player player = (Player) event.getWhoClicked();

        Skills skills = new Skills();

        //allows for toggling of the flower-drop feature.
        if(event.getCurrentItem().getType() == Material.RED_ROSE){
            cfgm.getPlayers().set("skills." + event.getWhoClicked().getUniqueId() + ".farming.flower-drop", !(cfgm.getPlayers().getBoolean("skills." + event.getWhoClicked().getUniqueId() + ".farming.flower-drop")));
            cfgm.savePlayers();
            skills.newInventory(player);

        }
    }

    private void dropssettings(InventoryClickEvent event){
        Material item = event.getCurrentItem().getType();
        Player player = (Player) event.getWhoClicked();
        ConfigManager cfgm = new ConfigManager();
        cfgm.Settingssetup();
        boolean clicked = false;

        //back button
        if(event.getCurrentItem().getType() == Material.WOOL){
            Settings settings = new Settings();
            settings.newInventory((Player) event.getWhoClicked());
            return;
        }

        //if statements identifying each click type and modifies the correct variable.
        if(event.getClick().isLeftClick() && event.getClick().isShiftClick()) {
            cfgm.getSettings().set("drops.seeds." + event.getCurrentItem().getType(), cfgm.getSettings().getInt("drops.seeds." + event.getCurrentItem().getType()) + 1);
            clicked = true;
        }
        if(event.getClick().isRightClick() && event.getClick().isShiftClick() && !clicked && cfgm.getSettings().getInt("drops.seeds." + event.getCurrentItem().getType()) > 0){
            cfgm.getSettings().set("drops.seeds." + event.getCurrentItem().getType(), cfgm.getSettings().getInt("drops.seeds." + event.getCurrentItem().getType()) - 1);
            clicked = true;
        }
        if(event.getClick().isLeftClick() && !clicked){
            cfgm.getSettings().set("drops.crops." + event.getCurrentItem().getType(), cfgm.getSettings().getInt("drops.crops." + event.getCurrentItem().getType()) + 1);
            clicked = true;
        }
        if(event.getClick().isRightClick() && !clicked && cfgm.getSettings().getInt("drops.crops." + event.getCurrentItem().getType()) > 0){
            cfgm.getSettings().set("drops.crops." + event.getCurrentItem().getType(), cfgm.getSettings().getInt("drops.crops." + event.getCurrentItem().getType()) - 1);
        }

        cfgm.saveSettings();
        SettingsDrops settings = new SettingsDrops();
        settings.newInventory((Player) event.getWhoClicked());
    }

    private void settings(InventoryClickEvent event){
        Material item = event.getCurrentItem().getType();
        Player player = (Player) event.getWhoClicked();
        ConfigManager cfgm = new ConfigManager();
        cfgm.Settingssetup();

        switch(item){
            case ENDER_PEARL: SettingsExperience experience = new SettingsExperience(); experience.newInventory(player); break;
            case WHEAT: SettingsDrops drops = new SettingsDrops(); drops.newInventory(player); break;
            case SOIL: cfgm.getSettings().set("crop-trample", !(cfgm.getSettings().getBoolean("crop-trample"))); cfgm.saveSettings(); Settings settings = new Settings(); settings.newInventory((Player) event.getWhoClicked());
        }
    }
}

package com.wrynn.farmingplugin.listeners;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPistonExtendEvent;

public class PistonExtend implements Listener {
    @EventHandler
    public void onPistonExtend(BlockPistonExtendEvent event){

        //counts all blocks pistonextendevent affects and checks whether they're a crop - cancels event if true.
        for (Block block : event.getBlocks().toArray(new Block[0])) {
            Material blocktype = block.getType();
            if (blocktype == Material.CROPS || blocktype == Material.CARROT || blocktype == Material.POTATO || blocktype == Material.BEETROOT_BLOCK || blocktype == Material.CACTUS || blocktype == Material.MELON_BLOCK || blocktype == Material.SUGAR_CANE_BLOCK || blocktype == Material.COCOA || blocktype == Material.NETHER_WARTS) {
                event.setCancelled(true);
                return;
            }
        }
    }
}

package com.wrynn.farmingplugin.listeners;

import net.minecraft.server.v1_12_R1.IChatBaseComponent;
import net.minecraft.server.v1_12_R1.PacketPlayOutTitle;
import net.minecraft.server.v1_12_R1.PlayerConnection;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import com.wrynn.farmingplugin.ConfigManager;
import com.wrynn.farmingplugin.LevelManager;
import com.wrynn.farmingplugin.Plants;
import com.wrynn.farmingplugin.SpecialEvents;

import java.util.Random;

public class BlockBreak implements Listener{

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event){

        ConfigManager cfgm = new ConfigManager();
        cfgm.Playerssetup();
        cfgm.Settingssetup();
        SpecialEvents sp = new SpecialEvents();
        Material blocktype = event.getBlock().getType();
        boolean isplant;

        //checks to see whether block broken is a crop or not
        switch(blocktype){
            case CROPS: case POTATO: case CARROT: case BEETROOT_BLOCK: case MELON_BLOCK: case PUMPKIN: case SUGAR_CANE_BLOCK: case CACTUS: case COCOA: case NETHER_WARTS: isplant = true; break;
            default: isplant = false; break;
        }

        if(isplant){
            Plants plant = new Plants(blocktype);
            boolean skip;

            // allows crops with no growthstates to skip the check for maximum growthstate
            switch(blocktype){
                case PUMPKIN: case CACTUS: case SUGAR_CANE_BLOCK: case MELON_BLOCK: skip = true; break;
                default: skip = false; break;
            }

            if(((plant.getMaxGrowthStage() <= event.getBlock().getData()) || skip) && event.getPlayer().getGameMode() == GameMode.SURVIVAL){

                event.setDropItems(false);
                Material iteminhand = event.getPlayer().getInventory().getItemInMainHand().getType();;
                Player player = event.getPlayer();

                //checks to see whether the material in hand in a hoe.
                if(iteminhand == Material.WOOD_HOE || iteminhand == Material.STONE_HOE || iteminhand == Material.IRON_HOE || iteminhand == Material.GOLD_HOE || iteminhand == Material.DIAMOND_HOE){

                    int numberofseeds = cfgm.getSettings().getInt("drops.seeds." + iteminhand.toString());
                    int numberofitems = cfgm.getSettings().getInt("drops.crops." + iteminhand.toString());
                    addexperience(blocktype, event.getPlayer());
                    sp.getRandomBreakEvent(event, iteminhand);

                    //prevents placing down sugarcane/cactus blocks and then immediately farming and getting more crops.
                    if(blocktype == Material.SUGAR_CANE_BLOCK && event.getBlock().getLocation().subtract(0,1,0).getBlock().getType() != Material.SUGAR_CANE_BLOCK && event.getBlock().getLocation().add(0,1,0).getBlock().getType() != Material.SUGAR_CANE_BLOCK){
                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), plant.getItem());
                        return;
                    }
                    if(blocktype == Material.CACTUS && event.getBlock().getLocation().subtract(0,1,0).getBlock().getType() != Material.CACTUS && event.getBlock().getLocation().add(0,1,0).getBlock().getType() != Material.CACTUS){
                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), plant.getItem());
                        return;
                    }

                    //prevents placing down pumpkin block or melon block and immediately harvesting for more crops.
                    if(blocktype == Material.PUMPKIN){
                        boolean besidestem = false;
                        for (BlockFace blockface : BlockFace.values()) {
                            if (event.getBlock().getRelative(blockface).getType() == Material.PUMPKIN_STEM) {
                                besidestem = true;
                            }
                        }
                        if(besidestem == false){
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), plant.getItem());
                            return;
                        }
                    }
                    if(blocktype == Material.MELON_BLOCK){
                        boolean besidestem = false;
                        for (BlockFace blockface : BlockFace.values()) {
                            if (event.getBlock().getRelative(blockface).getType() == Material.MELON_STEM) {
                                besidestem = true;
                            }
                        }
                        if(besidestem == false){
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), plant.getItem());
                            return;
                        }
                    }

                    //drops flowers if hoe used is not wood
                    if(iteminhand != Material.WOOD_HOE)
                        plant.DropFlowers(event);

                    //checks for enchantments on the tool and applies a modifier to the drops if one is present.
                    double modifier = 1;
                    int fortunelevel = player.getInventory().getItemInMainHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS);
                    switch(fortunelevel){
                        case 1: modifier = 1.25; break;
                        case 2: modifier = 1.5; break;
                        case 3: modifier = 1.75; break;
                    }

                    numberofitems = (int) Math.round(numberofitems * modifier);
                    numberofseeds = (int) Math.round(numberofseeds * modifier);

                    //accounts for the blocks above cactus and sugarcane blocks.
                    int multiplier = 1;
                    if(blocktype == Material.CACTUS || blocktype == Material.SUGAR_CANE_BLOCK)
                        multiplier = countblocksabove(event) + 1;

                    //for loop, runs as many items that drop and only allows blocks that have seeds to actually drop seeds
                    if(blocktype == Material.CROPS || blocktype == Material.BEETROOT_BLOCK || blocktype == Material.PUMPKIN || blocktype == Material.MELON_BLOCK){
                        for(int i = 0; i < numberofseeds * multiplier; i++)
                            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), plant.getSeeds());
                    }

                    for(int i = 0; i < numberofitems * multiplier; i++)
                        event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), plant.getItem());

                    int unbreakinglevel = player.getInventory().getItemInMainHand().getEnchantmentLevel(Enchantment.DURABILITY);

                    Random rnd = new Random();
                    if((unbreakinglevel == 0 || rnd.nextInt(unbreakinglevel) == 0))
                        event.getPlayer().getInventory().getItemInMainHand().setDurability((short)(event.getPlayer().getInventory().getItemInMainHand().getDurability() + 1));

                    if(event.getPlayer().getInventory().getItemInMainHand().getDurability() >= event.getPlayer().getInventory().getItemInMainHand().getType().getMaxDurability())
                        event.getPlayer().getInventory().getItemInMainHand().setAmount(0);

                }
                else{
                    event.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "You cannot break crops without a hoe!");
                }
            }
        }

        //prevents players from breaking the log cocoa beans are growing on
        if(blocktype == Material.LOG){
            for (BlockFace blockface : BlockFace.values())
                if (event.getBlock().getRelative(blockface).getType() == Material.COCOA && event.getBlock().getRelative(blockface).getData() >= 8){
                    event.setCancelled(true);
                    event.getPlayer().sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "You cannot break logs with cocoa beans growing on them. Please break the cocoa beans first.");
                }
        }

        //prevents players from breaking the block beneath crops to harvest them
        if(blocktype == Material.SOIL || blocktype == Material.SAND || blocktype == Material.GRASS || blocktype == Material.SOUL_SAND){
            Material blockabove = event.getBlock().getLocation().add(0,1,0).getBlock().getType();
            if(blockabove == Material.CROPS || blockabove == Material.SUGAR_CANE_BLOCK || blockabove == Material.CACTUS || blockabove == Material.POTATO || blockabove == Material.CARROT || blockabove == Material.BEETROOT_BLOCK || blockabove == Material.NETHER_WARTS){
                event.setCancelled(true);
                event.getPlayer().sendMessage(ChatColor.RED +""+ ChatColor.BOLD + "You cannot break blocks with crops above them. Please break the crop first. ");
            }
        }
    }

    //method adds experience to the player's players config file.
    private void addexperience(Material material, Player player){
        LevelManager lvlm = new LevelManager(player.getUniqueId());
        ConfigManager cfgm = new ConfigManager();
        cfgm.Settingssetup();

        lvlm.addExperience(cfgm.getSettings().getInt("experience." + material.toString()));

        if(lvlm.checkLevel()){
            PacketPlayOutTitle leveluptitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, IChatBaseComponent.ChatSerializer.a("{\"text\":\"You are now a level " + lvlm.getLevel() + " farmer!\",\"color\":\"gold\",\"bold\":true}"), 20, 40 , 30);
            PlayerConnection connection = ((CraftPlayer)player).getHandle().playerConnection;
            connection.sendPacket(leveluptitle);
        }
    }

    //accounts for similar blocks above the block broken. (used for sugarcane and cactus)
    private int countblocksabove(BlockBreakEvent event){
        int blockcount = 0;
        if(event.getBlock().getLocation().add(0,1,0).getBlock().getType() == event.getBlock().getType())
            blockcount = blockcount + 1;
        if(event.getBlock().getLocation().add(0,2,0).getBlock().getType() == event.getBlock().getType())
            blockcount = blockcount + 1;

        return blockcount;
    }
}

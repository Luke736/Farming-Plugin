package com.wrynn.farmingplugin.listeners;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.wrynn.farmingplugin.ConfigManager;

public class PlayerInteract implements Listener {

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event){
        ConfigManager cfgm = new ConfigManager();
        cfgm.Settingssetup();

        //stops crop trampling if set to 'false' in settings config.
        if(event.getAction() == Action.PHYSICAL && event.getClickedBlock().getType() == Material.SOIL && !(cfgm.getSettings().getBoolean("crop-trample")))
            event.setCancelled(true);
    }
}
